# gradle-script

#### 介绍
抽取日常开发中通用的Gradle脚本，不限于语言，限于Gradle构建的项目


#### 使用说明

1.  在根项目的build.gradle中增加如下代码，进行导入

```
  ext{
     gradleScriptHome = "https://gitee.com/ltalex/gradle-script/raw/master/"
  }
```

2.  应用脚本即可

```
apply from: gradleScriptHome + 'spring-version.gradle'
```
3. 结合1和2，你的项目代码应用应该是下面这样的

```
// buildscript 不能抽取出来，只能重复写。
buildscript {
    ext{
        gradleScriptHome = "https://gitee.com/ltalex/gradle-script/raw/master/"
    }
    apply from: gradleScriptHome + 'maven.gradle'
    apply from: gradleScriptHome + 'spring-version.gradle'
    repositories {
        maven { url REPOSITORY_URL }
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
    }
}

apply from: gradleScriptHome + 'spring-cloud-alibaba.gradle'


group = 'com.yeamin.basedata' // 设置group id
version = '0.0.1-SNAPSHOT' // 设置版本
description = 'xxx-service' // 设置描述
sourceCompatibility = '1.8'


jar {
    manifest {
        attributes 'Main-Class': 'com.pureland.gateway.BaseDataApplication'
    }
}


// 依赖项
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-web'
    implementation 'com.alibaba.cloud:spring-cloud-starter-alibaba-nacos-discovery'
    testImplementation('org.springframework.boot:spring-boot-starter-test') {
        exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
}

test {
    useJUnitPlatform()
}
```
#### 联系我们

1. 邮箱: lt_alex@163.com
2. QQ: 1301015490